#!/bin/bash

. ./checkenv.sh

if [ "${exit_code}" -eq 0 ]; then
  # Convert ENVIRONMENT env variable to lower case
  ENV=$(echo ${ENVIRONMENT} | tr A-Z a-z)
  echo "Starting service ${SERVICE} for ${ENV}"
  ACTIVE_PROFILES=${ENV},consul
  java -jar service.jar --spring.profiles.active=${ACTIVE_PROFILES}
else
  echo "Unable to start service ${SERVICE} for ${ENV} --> check dependencies and retry"
fi