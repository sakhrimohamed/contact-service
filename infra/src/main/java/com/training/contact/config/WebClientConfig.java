package com.training.contact.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.ReactiveOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ReactiveClientRegistrationRepository;
import org.springframework.security.oauth2.client.web.reactive.function.client.ServerOAuth2AuthorizedClientExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Web configuration.
 */
@Configuration
public class WebClientConfig {

    @Bean
    public WebClient keycloakWebClient(ReactiveClientRegistrationRepository clientRegistrations,
                                       ReactiveOAuth2AuthorizedClientService authorizedClientService,
                                       @Value("${keycloak-client.server-url}") String adminBaseUrl) {
        AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager clientManager
                = new AuthorizedClientServiceReactiveOAuth2AuthorizedClientManager(clientRegistrations, authorizedClientService);
        ServerOAuth2AuthorizedClientExchangeFilterFunction oauth = new ServerOAuth2AuthorizedClientExchangeFilterFunction(clientManager);
        oauth.setDefaultClientRegistrationId("keycloak");
        return WebClient.builder()
                .filter(oauth)
                .baseUrl(adminBaseUrl)
                .build();
    }
}
