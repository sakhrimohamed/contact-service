package com.training.contact.rest.advice;


import com.training.contact.manage.exception.NotAllowedOperationException;
import com.training.contact.manage.exception.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * The type Global controller exception handler.
 */
@RestControllerAdvice
public class GlobalControllerExceptionHandler {


    @ExceptionHandler({NotAllowedOperationException.class, UserNotFoundException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ResponseEntity handle(RuntimeException ex) {
        return new ResponseEntity(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

}