package com.training.contact.rest;

import com.github.dozermapper.core.Mapper;
import com.training.contact.manage.exception.UserNotFoundException;
import com.training.contact.manage.model.Contact;
import com.training.contact.manage.spi.UserService;
import com.training.contact.mapping.GenericObjectMapper;
import com.training.contact.mapping.impl.GenericObjectMapperImpl;
import com.training.contact.manage.api.ContactOperationHandler;
import com.training.contact.rest.dao.ContactDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Slf4j
@RestController
@RequestMapping("/api/v1")
public class ContactOperationsRestController {

    private final ContactOperationHandler contactOperationHandler;
    private final UserService userService;
    private final GenericObjectMapper<ContactDao, Contact> contactInDataMapper;
    private final GenericObjectMapper<Contact, ContactDao> contactOutDataMapper;

    public ContactOperationsRestController(ContactOperationHandler contactOperationHandler,
                                           UserService userService,
                                           Mapper objectMapper) {
        this.contactOperationHandler = contactOperationHandler;
        this.userService = userService;
        this.contactOutDataMapper = new GenericObjectMapperImpl<>(objectMapper, ContactDao.class);
        this.contactInDataMapper = new GenericObjectMapperImpl<>(objectMapper, Contact.class);
    }

    @GetMapping("users/{userName}/contacts")
    public Flux<ContactDao> getContacts(@PathVariable("userName") String userName) {
        log.info("#### Received request for getting all contacts for user {}", userName);
        return validateUser(userName)
                .thenMany(contactOperationHandler.listContacts(userName).map(contactOutDataMapper::convert));
    }

    @GetMapping("users/{userName}/contacts/{contactId}")
    public Mono<ContactDao> getContactDetails(@PathVariable("userName") String userName,
                                              @PathVariable("contactId") Long contactId) {
        log.info("#### Received request for getting contact details for code {} and user {}", contactId, userName);
        return validateUser(userName)
                .then(contactOperationHandler.showContactDetails(contactId, userName).map(contactOutDataMapper::convert));
    }

    @PostMapping("users/{userName}/contacts")
    public Mono<ContactDao> createContact(@PathVariable("userName") String userName,
                                          @RequestBody ContactDao contactDao) {
        log.info("#### Received request to create contact for user {}", userName);
        return validateUser(userName)
                .then(contactOperationHandler.createContact(userName, contactInDataMapper.convert(contactDao))
                        .map(contactOutDataMapper::convert));
    }

    @PutMapping("users/{userName}/contacts/{contactId}")
    public Mono<ContactDao> updateContact(@PathVariable("userName") String userName,
                                          @PathVariable("contactId") Long contactId,
                                          @RequestBody ContactDao contactDao) {
        log.info("#### Received request to update contact with code {} for user {}", contactId, userName);
        return validateUser(userName)
                .then(contactOperationHandler.updateContact(userName, contactId, contactInDataMapper.convert(contactDao))
                        .map(contactOutDataMapper::convert));
    }

    @DeleteMapping("users/{userName}/contacts/{contactId}")
    public Mono<Boolean> deleteContact(@PathVariable("userName") String userName,
                                          @PathVariable("contactId") Long contactId) {
        log.info("#### Received request to delete contact with code {} for user {}", contactId, userName);
        return validateUser(userName)
                .then(contactOperationHandler.deleteContact(userName, contactId));
    }

    private Mono<Boolean> validateUser(String userName) {
        return userService.getUserByUserName(userName)
                .flatMap(retreivedUserOpt -> {
                    if(retreivedUserOpt.isEmpty())
                        return Mono.error(new UserNotFoundException(userName));
                    return Mono.just(true);
                });
    }
}
