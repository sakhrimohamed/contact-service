package com.training.contact.rest.dao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContactDao {
    private String id;
    private String firstName;
    private String lastName;
    private String email;

}
