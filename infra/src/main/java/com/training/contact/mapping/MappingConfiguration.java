package com.training.contact.mapping;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.github.dozermapper.core.loader.api.BeanMappingBuilder;
import com.training.contact.manage.model.Contact;
import com.training.contact.entity.ContactEntity;
import com.training.contact.rest.dao.ContactDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MappingConfiguration {

    @Bean
    public Mapper objectMapper() {

        // contract builder to define mapping
        BeanMappingBuilder builder = new BeanMappingBuilder() {
            @Override
            protected void configure() {

                mapping(Contact.class, ContactEntity.class);
                mapping(ContactDao.class, Contact.class);


            }
        };
        return DozerBeanMapperBuilder.create().withMappingBuilder(builder).build();
    }
}
