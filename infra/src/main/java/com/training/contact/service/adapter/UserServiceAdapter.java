package com.training.contact.service.adapter;

import com.training.contact.manage.spi.UserService;
import com.training.contact.manage.model.UserResource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Service
@Slf4j
public class UserServiceAdapter implements UserService {

    private static final String ADMIN_SEGMENT = "admin";
    private static final String REALMS_SEGMENT = "realms";

    private static final String USERS_SEGMENT = "users";

    private static final String REALM = "givaudan";
    private static final String USERNAME = "username";
    private static final String EXACT = "exact";

    private final WebClient keycloakWebClient;

    public UserServiceAdapter(WebClient keycloakWebClient) {
        this.keycloakWebClient = keycloakWebClient;
    }

    @Override
    public Mono<Optional<UserResource>> getUserByUserName(String userId){
        log.debug("### Calling Keycloak to find user by username {} ", userId);
        return keycloakWebClient.get()
                .uri(u ->
                        u.pathSegment(ADMIN_SEGMENT, REALMS_SEGMENT, REALM, USERS_SEGMENT)
                                .queryParam(USERNAME, userId)
                                .queryParam(EXACT, true)
                                .build()
                )
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(UserResource.class)
                .collectList()
                .handle((users, sink) -> {
                    if(users.isEmpty()){
                        log.debug("### Not Found User with usernamer {} ", userId);
                        sink.next(Optional.empty());
                    } else {
                        log.debug("### User with usernamer {} successfully found ", userId);
                        sink.next(Optional.of(users.get(0)));
                    }
                });
    }

}
