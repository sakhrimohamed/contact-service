package com.training.contact.service.adapter;

import com.training.contact.manage.business.ServiceFactory;
import com.training.contact.repository.adapter.ContactRepositoryAdapter;
import com.training.contact.manage.api.ContactOperationHandler;
import com.training.contact.manage.model.Contact;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ContactOperationHandlerAdapter implements ContactOperationHandler {

    private final ContactOperationHandler contactOperationHandler;

    public ContactOperationHandlerAdapter(ContactRepositoryAdapter contactRepositoryAdapter) {
        this.contactOperationHandler = ServiceFactory.getInstance()
                .contactOperationHandler(contactRepositoryAdapter);
    }

    @Override
    public Flux<Contact> listContacts(String userCode) {
        return contactOperationHandler.listContacts(userCode);
    }

    @Override
    public Mono<Contact> showContactDetails(Long contactId, String userName) {
        return contactOperationHandler.showContactDetails(contactId, userName );
    }

    @Override
    public Mono<Contact> createContact(String userCode, Contact contact) {
        return contactOperationHandler.createContact(userCode, contact);
    }

    @Override
    public Mono<Contact> updateContact(String userCode, Long contactId, Contact contact) {
        return contactOperationHandler.updateContact(userCode, contactId, contact);
    }

    @Override
    public Mono<Boolean> deleteContact(String userCode, Long contactId) {
        return contactOperationHandler.deleteContact(userCode, contactId);
    }

    @Override
    public Flux<String> importContacts(String userCode, Flux<Contact> contactCodes) {
        return contactOperationHandler.importContacts(userCode, contactCodes);
    }

    @Override
    public Flux<Contact> exportContacts(String userCode, Flux<String> contactCodes) {
        return contactOperationHandler.exportContacts(userCode, contactCodes);
    }
}
