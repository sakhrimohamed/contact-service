package com.training.contact.repository.adapter;

import com.github.dozermapper.core.Mapper;
import com.training.contact.entity.ContactEntity;
import com.training.contact.mapping.GenericObjectMapper;
import com.training.contact.mapping.impl.GenericObjectMapperImpl;
import com.training.contact.manage.model.Contact;
import com.training.contact.manage.spi.ContactRepository;
import com.training.contact.repository.ContactEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Slf4j
@Service
public class ContactRepositoryAdapter implements ContactRepository {

    private final ContactEntityRepository contactEntityRepository;
    private final GenericObjectMapper<Contact, ContactEntity> contactInDataMapper;
    private final GenericObjectMapper<ContactEntity, Contact> contactOutDataMapper;

    public ContactRepositoryAdapter(ContactEntityRepository contactEntityRepository,
                                    Mapper objectMapper) {
        this.contactEntityRepository = contactEntityRepository;
        this.contactInDataMapper = new GenericObjectMapperImpl<>(objectMapper, ContactEntity.class);
        this.contactOutDataMapper = new GenericObjectMapperImpl<>(objectMapper, Contact.class);
    }

    @Override
    public Mono<Contact> saveContact(Contact contact) {
        return Mono.just(contactEntityRepository.save(contactInDataMapper.convert(contact)))
                .map(contactOutDataMapper::convert);
    }

    @Override
    public Mono<Contact> getContactDetails(Long contactId) {
        return contactEntityRepository.findById(contactId)
                .map(contactEntity -> Mono.just(contactOutDataMapper.convert(contactEntity)))
                .orElseGet(Mono::empty);
    }

    @Override
    public Mono<Boolean> removeContact(Long contactId) {
        contactEntityRepository.deleteById(contactId);
        return Mono.just(true);
    }

    @Override
    public Flux<Contact> listContact(String userId) {
        return Flux.fromIterable(contactEntityRepository.findByUserId(userId))
                .map(contactOutDataMapper::convert);
    }
}
