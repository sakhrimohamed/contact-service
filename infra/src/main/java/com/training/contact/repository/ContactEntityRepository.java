package com.training.contact.repository;

import com.training.contact.entity.ContactEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactEntityRepository extends CrudRepository<ContactEntity, Long> {

	List<ContactEntity> findByUserId(String userCode);
}