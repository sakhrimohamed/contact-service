package com.training.contact.entity;

import jakarta.persistence.*;
import lombok.Data;


@Entity
@Table(name = "CONTACT")
@Data
public class ContactEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
}
