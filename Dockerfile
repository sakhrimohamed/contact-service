FROM adoptopenjdk/openjdk15:jdk-15.0.2_7-debian

ENV SERVICE=Contact-Service

ENV LOG_DIR=/home/logs

ENV LOG_PROFILE=file-log,console-log

WORKDIR /var/lib

COPY infra/target/contact-service.jar ./service.jar
COPY start.sh .

ENTRYPOINT ["sh" ,"start.sh"]
