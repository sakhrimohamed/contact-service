Feature: contact operations

  Scenario: create contact
    Given have user with userName sakhri
    When create contact for user with userName sakhri with firstName Mohamed and lastName sakhri and email msahri@gmail.com
    Then contact should be created with firstName Mohamed and lastName sakhri and email msahri@gmail.com for user with userName sakhri
