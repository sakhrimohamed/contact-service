package contact.operations.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * The type Cucumber test.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = {"src/test/resources/features"},
        tags = "not @Ignore",
        glue = "contact.operations.cucumber.stepdefinitions")
public class RunCucumberTest {
}
