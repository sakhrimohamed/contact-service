package contact.operations.cucumber.stepdefinitions;

import com.training.contact.manage.business.ContactOperationHandlerImpl;
import com.training.contact.manage.business.ServiceFactory;
import com.training.contact.manage.model.Contact;
import com.training.contact.manage.spi.ContactRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StepDef {

    private final ContactRepository contactRepository = mock(ContactRepository.class);
    private List<Contact> contactList = new ArrayList<>();

    @Given("^have user with userName (.*)$")
    public void initUser(String userName) {
        when(contactRepository.saveContact(any(Contact.class))).thenAnswer(i -> Mono.just(i.getArguments()[0]));
    }

    @When("^create contact for user with userName (.*) with firstName (.*) and lastName (.*) and email (.*)$")
    public void createContact(String userName, String firstName, String lastName, String email) {
        ContactOperationHandlerImpl handler = ServiceFactory.getInstance().contactOperationHandler(contactRepository);
        var contact  = Contact.builder()
                .userId(userName)
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .build();
        var res = handler.createContact(userName, contact).block();
        contactList.add(res);
    }

    @Then("^contact should be created with firstName (.*) and lastName (.*) and email (.*) for user with userName (.*)$")
    public void verifyCreatedContact(String firstName, String lastName, String email, String userName) {
        var contact  = Contact.builder()
                .userId(userName)
                .firstName(firstName)
                .lastName(lastName)
                .email(email)
                .build();
        Assertions.assertTrue(contactList.contains(contact));
    }
}
