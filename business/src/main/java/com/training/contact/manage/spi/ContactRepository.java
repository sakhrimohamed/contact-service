package com.training.contact.manage.spi;

import com.training.contact.manage.model.Contact;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ContactRepository {
    Mono<Contact> saveContact(Contact contact);
    Mono<Contact> getContactDetails(Long contactId);
    Mono<Boolean> removeContact(Long contactId);
    Flux<Contact> listContact(String userId);
}
