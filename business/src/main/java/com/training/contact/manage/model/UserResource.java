package com.training.contact.manage.model;

import lombok.Data;

@Data
public class UserResource {
    private String username;
    private String firstName;
    private String lastName;
    private boolean enabled;
}