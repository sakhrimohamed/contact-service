package com.training.contact.manage.business;

import com.training.contact.manage.exception.NotAllowedOperationException;
import com.training.contact.manage.spi.ContactRepository;
import com.training.contact.manage.api.ContactOperationHandler;
import com.training.contact.manage.model.Contact;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * A business implementaion of {@link ContactOperationHandler}
 */
@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Slf4j
public class ContactOperationHandlerImpl implements ContactOperationHandler {

    public static final String NOT_ALLOWED_ERROR_MSG = "Not allowed operation for user '%s' and account code '%s' ";

    private final ContactRepository contactRepository;

    @Override
    public Flux<Contact> listContacts(String userId) {
        return contactRepository.listContact(userId);
    }

    @Override
    public Mono<Contact> showContactDetails(Long contactId, String userName) {
        return contactRepository.getContactDetails(contactId)
                .flatMap(contactDetails -> validateUserName(userName, contactDetails));
    }

    @Override
    public Mono<Contact> createContact(String userId, Contact contact) {
        contact.setUserId(userId);
        return contactRepository.saveContact(contact);
    }

    @Override
    public Mono<Contact> updateContact(String userName, Long contactId, Contact contact) {
        return contactRepository.getContactDetails(contactId)
                .flatMap(contactDetails -> validateUserName(userName, contactDetails))
                .flatMap(retreivedContact -> updateContactDetails(contact, retreivedContact))
                .flatMap(contactRepository::saveContact);
    }

    @Override
    public Mono<Boolean> deleteContact(String userName, Long contactId) {
        return contactRepository.getContactDetails(contactId)
                .flatMap(contactDetails -> validateUserName(userName, contactDetails))
                .flatMap(contact -> contactRepository.removeContact(contactId));
    }

    private Mono<Contact> validateUserName(String userName, Contact contact) {
        if(contact.getUserId().equals(userName))
            return Mono.just(contact);
        return Mono.error(new NotAllowedOperationException(String.format(NOT_ALLOWED_ERROR_MSG, userName, contact.getId())));
    }

    private Mono<Contact> updateContactDetails(Contact newContact, Contact retreivedContact) {
        newContact.setId(retreivedContact.getId());
        newContact.setUserId(retreivedContact.getUserId());
        return Mono.just(newContact);
    }

    @Override
    public Flux<String> importContacts(String userName, Flux<Contact> contactCodes) {
        return null;
    }

    @Override
    public Flux<Contact> exportContacts(String userName, Flux<String> contactCodes) {
        return null;
    }


}
