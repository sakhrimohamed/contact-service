package com.training.contact.manage.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Contact {
    private Long id;
    private String userId;
    private String firstName;
    private String lastName;
    private String email;
}
