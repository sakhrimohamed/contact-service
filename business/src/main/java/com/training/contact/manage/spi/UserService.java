package com.training.contact.manage.spi;

import com.training.contact.manage.model.UserResource;
import reactor.core.publisher.Mono;

import java.util.Optional;

public interface UserService {
    Mono<Optional<UserResource>> getUserByUserName(String userId);
}
