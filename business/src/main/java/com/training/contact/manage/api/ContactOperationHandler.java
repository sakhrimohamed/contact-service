package com.training.contact.manage.api;

import com.training.contact.manage.model.Contact;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * API used to manage contacts (create, update, list, remove, show details)
 */
public interface ContactOperationHandler {

    Flux<Contact> listContacts(String userCode);
    Mono<Contact> showContactDetails(Long contactId, String userName);
    Mono<Contact> createContact(String userCode, Contact  contact);
    Mono<Contact> updateContact(String userCode, Long contactId, Contact contact);
    Mono<Boolean> deleteContact(String userCode, Long contactId);
    Flux<String> importContacts(String userCode, Flux<Contact> contactCodes);
    Flux<Contact> exportContacts(String userCode, Flux<String> contactCodes);

}
