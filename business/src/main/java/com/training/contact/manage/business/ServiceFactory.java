package com.training.contact.manage.business;

import com.training.contact.manage.spi.ContactRepository;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ServiceFactory {

    @Getter
    private static final ServiceFactory Instance = new ServiceFactory();

    public ContactOperationHandlerImpl contactOperationHandler(ContactRepository contactRepository){
        return new ContactOperationHandlerImpl(contactRepository);
    }

}
