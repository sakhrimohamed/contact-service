package com.training.contact.manage.exception;

/**
 * A user not found exception.
 */
public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException( String username) {
        super("User '" + username  + "' does not exist");
    }
}
